/*
---> pin config

  2 => led
  3 => mode select
  A4 => SDA
  A5 => SDL

*/
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 RTC;
LiquidCrystal_I2C lcd(0x3f, 16, 2);
const int mode_pin = 3;
const int led_pin = 2;
const int hum_pin = 0;

void setup() {
    Serial.begin(9600);
    lcd.begin();
    pinMode(mode_pin,INPUT);
    pinMode(led_pin,OUTPUT);
    pinMode(hum_pin,INPUT);

    Wire.begin();
    RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}

void loop() {

byte IRRIG = 0;//activate irrigation
byte HUM_SENSOR = 0;

lcd.clear();

if(digitalRead(mode_pin) == LOW){
  Serial.println("Mode 0");
  DateTime now = RTC.now();

  lcd.setCursor(0, 0);
  lcd.print("Mode 0");
  lcd.print(" (");
  lcd.print(now.hour());
  lcd.print(":");
  lcd.print(now.minute());
  lcd.print(")");


  Serial.print(now.year(), DEC);Serial.print('/');Serial.print(now.month(), DEC);Serial.print('/');Serial.print(now.day(), DEC);Serial.print(' ');Serial.print(now.hour(), DEC);Serial.print(':');Serial.print(now.minute(), DEC);Serial.print(':');Serial.print(now.second(), DEC);Serial.println();

  if(now.hour() == 0){//activer l'irr pendant 1 heure
    IRRIG = 1;
  }
}else{
  Serial.println("Mode 1");
  HUM_SENSOR = map(analogRead(hum_pin),0,1023,100,0);
  lcd.setCursor(0, 0);
  lcd.print("Mode 1");
  lcd.print(" (");
  lcd.print(HUM_SENSOR);
  lcd.print("%");
  lcd.print(")");


  //Serial.println(HUM_SENSOR);
  if(HUM_SENSOR < 20) IRRIG = 1;
  }

if(IRRIG){
    Serial.println("Irrigation Start");
    lcd.setCursor(0, 1);
    lcd.print("Irr Start");
    digitalWrite(led_pin,HIGH);
  }else{
    Serial.println("Irrigation Stop");
    lcd.setCursor(0, 1);
    lcd.print("Irr Stop");
    digitalWrite(led_pin,LOW);
  }
  delay(10);
}
